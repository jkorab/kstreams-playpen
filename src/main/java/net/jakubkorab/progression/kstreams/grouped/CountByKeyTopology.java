package net.jakubkorab.progression.kstreams.grouped;

import net.jakubkorab.progression.TopologyBuilder;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;

public class CountByKeyTopology implements TopologyBuilder {

    private final String sourceTopic;
    private final String targetTopic;

    public CountByKeyTopology(String sourceTopic, String targetTopic) {
        this.sourceTopic = sourceTopic;
        this.targetTopic = targetTopic;
    }

    @Override
    public Topology build() {
        StreamsBuilder builder = new StreamsBuilder();

        Serde<String> stringSerde = Serdes.String();
        Serde<Long> longSerde = Serdes.Long();

        builder.stream(sourceTopic, Consumed.with(stringSerde, stringSerde))
                .groupByKey()
                .count()
                .toStream()
                .to(targetTopic, Produced.with(stringSerde, longSerde));

        return builder.build();
    }

}
