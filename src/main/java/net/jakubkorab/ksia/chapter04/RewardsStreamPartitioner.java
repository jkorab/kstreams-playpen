package net.jakubkorab.ksia.chapter04;

import net.jakubkorab.ksia.model.Purchase;
import org.apache.kafka.streams.processor.StreamPartitioner;

public class RewardsStreamPartitioner implements StreamPartitioner<String, Purchase> {

    @Override
    public Integer partition(String topic, String s, Purchase purchase, int numPartitions) {
        // using a property of the value to determine the correct partition
        return purchase.getCustomerId().hashCode() % numPartitions;
    }

}
