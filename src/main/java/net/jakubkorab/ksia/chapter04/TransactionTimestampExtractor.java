package net.jakubkorab.ksia.chapter04;

import net.jakubkorab.ksia.model.Purchase;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;


public class TransactionTimestampExtractor implements TimestampExtractor {

    @Override
    public long extract(ConsumerRecord<Object, Object> record, long previousTimestamp) {
        Purchase purchase = (Purchase) record.value();
        return purchase.getPurchaseDate().getTime();
    }
}
